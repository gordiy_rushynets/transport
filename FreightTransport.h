//
//  FreightTransport.h
//  transport
//
//  Created by Gordiy Rushynets on 3/18/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef FreightTransport_h
#define FreightTransport_h

#include "Car.h"

using namespace std;

class FreightTransport : public Car
{
protected:
    int load_capacity;
public:
    FreightTransport():
    Car(),
    load_capacity(0),
    {}
    
    istream &input(istream &in) override
    {
        return in >> load_capacity;
    }
    
    ostream &output(ostream &out) override
    {
        return out << load_capacity;
    }
    
    friend istream &operator >> (istream &in, FreightTransport &ft)
    {
        return ft.input(in);
    }
    
    friend ostream &operator << (ostream &out, FreightTransport &ft)
    {
        return ft.output(out);
    }
    
    void set_load_capacity(int capacity)
    {
        load_capacity = capacity;
    }
    
    int get_load_capacity()
    {
        return load_capacity;
    }
    
};

#endif /* FreightTransport_h */
