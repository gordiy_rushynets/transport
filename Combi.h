//
//  Combi.h
//  transport
//
//  Created by Gordiy Rushynets on 3/18/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef Combi_h
#define Combi_h

#include <fstream>

#include "PassangerCar.h"
#include "FreightTransport.h"

class Combi: public FreightTransport, PassangerCar
{
private:
    string owner;
public:
    Combi():
    FreightTransport(),
    PassangerCar(),
    owner("")
    {}
    
    istream &input(istream &in) override
    {
        return in >> owner;
    }
    
    ostream &output(ostream &out) override
    {
        return out << owner;
    }
    
    friend istream &operator >> (istream &in, Combi &combi)
    {
        return combi.input(in);
    }
    
    friend ostream &operator << (ostream &out, Combi &combi)
    {
        return combi.output(out);
    }
    
    void set_owner(string car_owner)
    {
        owner = car_owner;
    }
    
    string get_owner()
    {
        return owner;
    }
};

#endif /* Combi_h */
