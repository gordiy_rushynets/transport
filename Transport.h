//
//  Transport.h
//  transport
//
//  Created by Gordiy Rushynets on 3/18/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef Transport_h
#define Transport_h

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

using namespace std;

class Transport
{
protected:
    int birth_year;
    string marka;
public:
    Transport():
    birth_year(0),
    marka(""){}
    
    void set_year(int year)
    {
        birth_year = year;
    }
    
    void set_marka(string new_marka)
    {
        marka = new_marka;
    }
    
    int get_year()
    {
        return birth_year;
    }
    
    string get_marka()
    {
        return marka;
    }
    
    virtual istream &input(istream &in) = 0;
    virtual ostream &output(ostream &out) = 0;
    
    friend istream &operator >> (istream &in, Transport &transport)
    {
        return transport.input(in);
    }
    
    friend ostream &operator << (ostream &out, Transport &transport)
    {
        return transport.output(out);
    }
};

#endif /* Transport_h */
