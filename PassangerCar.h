//
//  PassangerCar.h
//  transport
//
//  Created by Gordiy Rushynets on 3/18/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef PassangerCar_h
#define PassangerCar_h

#include "Car.h"
#include <fstream>

class PassangerCar : public Car
{
protected:
    int count_doors;
public:
    PassangerCar():
    Car(),
    count_doors(0),
    {}
    
    istream &input (istream &in) override
    {
        return in >> count_doors;
    }
    
    friend istream &operator >> (istream &in, PassangerCar &car)
    {
        return car.input(in);
    }
    
    ostream &output (ostream &out) override
    {
        return out << count_doors;
    }
    
    friend ostream &operator << (ostream &out, PassangerCar &c)
    {
        return c.output(out);
    }
    
    void set_number_of_doors(int num_of_doors)
    {
        count_doors = num_of_doors;
    }
    
    int get_number_of_doors()
    {
        return count_doors;
    }
    
};


#endif /* PassangerCar_h */
