//
//  Car.h
//  transport
//
//  Created by Gordiy Rushynets on 3/18/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#ifndef Car_h
#define Car_h

#include <string>
#include <fstream>

#include "Transport.h"

using namespace std;

class Car : public virtual Transport
{
protected:
    int max_speed;
    string model;
    double price;
public:
    Car():
    Transport(),
    max_speed(0),
    price(0),
    model(""){}
    
    istream &input(istream &in) override
    {
        return in >> max_speed >> model >> price;
    }
    
    ostream &output(ostream &out) override
    {
        return out << max_speed << ' ' << model << ' ' << price;
    }
    
    friend istream &operator >> (istream &in, Car &car)
    {
        return car.input(in);
    }
    
    friend ostream &operator << (ostream &out, Car &car)
    {
        return car.output(out);
    }
    
    void set_price(double car_price)
    {
        price = car_price;
    }
    
    void set_max_speed(int maximum_speed)
    {
        max_speed = maximum_speed;
    }
    
    void set_model(string car_model)
    {
        model =  car_model;
    }
    
    int get_max_speed()
    {
        return max_speed;
    }
    
    string get_model()
    {
        return model;
    }
    
    double get_price()
    {
        return price;
    }
    
};

#endif /* Car_h */
